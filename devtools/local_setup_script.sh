#!/usr/bin/env bash
set -m
set -e


PYTHONUNBUFFERED=1

./patches/patch.sh || true

./patches/override.sh `pwd`/src/sempo_extensions `pwd`/contrib/sempo || true
./patches/override.sh `pwd`/src/ussd_extensions `pwd`/contrib/ussd || true

if [ -z ${PGUSER+x} ]
then
echo "[WARN] PGUSER environment variable not set, defaulting to postgres user 'postgres'"
fi

if [ -z ${PGPASSWORD+x} ]
then
echo "[WARN] PGPASSWORD environment variable not set, defaulting to postgres password 'password'"
fi


echo "This will wipe ALL local Sempo data"

echo "Reset Local Secrets? y/N"
read resetSecretsInput

#echo "Persist Ganache? y/N"
#read ganachePersistInput

echo "Create Dev Data? (s)mall/(m)edium/(l)arge/(N)o"
read testDataInput

if [ "$testDataInput" == 's' ]; then
    echo "Will create Small Dev Dataset"
    testdata='small'
elif [ "$testDataInput" == 'm' ]; then
    echo "Will create Medium Dev Dataset"
    testdata='medium'
elif [ "$testDataInput" == 'l' ]; then
    echo "Will create Large Dev Dataset"
    testdata='large'
else
    echo "Will not create Dev Dataset"
    testdata='none'
fi

echo ~~~~Creating Secrets
pushd $SEMPO_PATH/config_files/
#python generate_secrets.py -m ${master_pk} -o ${owner_pk} -f ${float_pk}
python generate_secrets.py 
popd
[ ! -d ./src/common/config_files/secret ] && mkdir ./src/common/config_files/secret
pushd ./src/common/config_files
#python generate_secrets.py -m ${master_pk} -o ${owner_pk} -f ${float_pk}
python generate_secrets.py 
popd
#cp -r ./src/common/config_files/* ./contrib/sempo/config_files

echo ~~~~Killing any leftover workers or app
set +e
kill -9 $(ps aux | grep '[r]un.py' | awk '{print $2}')
kill -9 $(ps aux | grep '[c]elery' | awk '{print $2}')

echo ~~~~Resetting readis
#redis-server &
#redis-cli FLUSHALL

#sleep 1

echo ~~~~Resetting postgres
echo If this section hangs, you might have a bunch of idle postgres connections. Kill them using
echo "sudo kill -9 \$(ps aux | grep '[p]ostgres .* idle' | awk '{print \$2}')"

db_server=postgres://${PGUSER:-postgres}:${PGPASSWORD:-password}@localhost:5432
app_db=$db_server/${APP_DB:-grassroots}
eth_worker_db=$db_server/${WORKER_DB:-sempo_eth_worker}

echo ~~~~ App DB $app_db

set -e
psql $db_server -c ''
set +e

echo terminating existing connections to dbserver that may be locking the database
cat <<EOF | psql $db_server
SELECT    pg_terminate_backend (pid)
FROM    pg_stat_activity
WHERE    pg_stat_activity.datname = 'grassroots';
EOF

psql $db_server -c "DROP DATABASE IF EXISTS ${APP_DB:-grassroots}"
psql $db_server -c "DROP DATABASE IF EXISTS ${WORKER_DB:-sempo_eth_worker}"
psql $db_server -c "CREATE DATABASE ${APP_DB:-grassroots}"
psql $db_server -c "CREATE DATABASE ${WORKER_DB:-sempo_eth_worker}"

# temporary workaround to allow generating real secrets later
# the common secrets is always copied by the generate script from the template unchanged
mkdir -vp $SEMPO_PATH/config_files/secret
cp -v $SEMPO_PATH/config_files/templates/common_secrets_template.ini $SEMPO_PATH/config_files/secret/common_secrets.ini 

pushd $SEMPO_PATH/app
python manage.py db upgrade
popd


pushd $SEMPO_PATH/eth_worker/
alembic upgrade heads
popd

#kill $(ps aux | grep '[g]anache-cli' | awk '{print $2}')

#if [ "$ganachePersistInput" == 'b' ]
#if [ "$resetSecretsInput" == "y" ]; then
  echo ~~~~Resetting Ganache
  rm -rf $SEMPO_PATH/ganacheDB
  mkdir $SEMPO_PATH/ganacheDB
#  ganache-cli -l 80000000 -i 8995 -a 10 -e 10000000 --db "$SEMPO_PATH/ganacheDB" -m "history stumble mystery avoid embark arrive mom foil pledge keep grain dice" --account_keys_path "$SEMPO_PATH/ganacheDB/keys.json" &
#  ganache-cli -l 80000000 -i 8995 -a 10 -e 10000000 --db "$SEMPO_PATH/ganacheDB" -m "$DEV_MNEMONIC" --account_keys_path "$SEMPO_PATH/ganacheDB/keys.json" &
  ganache-cli -l 80000000 -i ${ETH_NETWORK_ID} -a 10 -e "${ETH_DEV_INITIAL_BALANCE}" --db "$SEMPO_PATH/ganacheDB" -m "$ETH_MNEMONIC" -v & #--account_keys_path "$SEMPO_PATH/ganacheDB/keys.json" &

  bip39gen -n 5 "$DEV_MNEMONIC" > keys.json
  pks=($(cat keys.json | jq -r '.private[]'))

#  sleep 5
#  _IFS=$IFS
#  IFS=$'\r\n'
#  pks=($(cat $SEMPO_PATH/ganacheDB/keys.json  | jq -r '.private_keys | values[]'))

  master_pk=${pks[0]}
  owner_pk=${pks[1]}
  float_pk=${pks[2]}

  echo "master key $master_pk"
  echo "onwer key $owner_pk"
  echo "float key $float_pk"

  sed -i "/^\[ETHEREUM\]$/,/^\[$/ s/^master_wallet_private_key[[:space:]].*$/master_wallet_private_key = 0x${master_pk}/" $SEMPO_PATH/config_files/secret/local_secrets.ini
  sed -i "/^\[ETHEREUM\]$/,/^\[$/ s/^owner_private_key[[:space:]].*$/owner_private_key = 0x${owner_pk}/" $SEMPO_PATH/config_files/secret/local_secrets.ini
  sed -i "/^\[ETHEREUM\]$/,/^\[$/ s/^float_private_key[[:space:]].*$/float_private_key = 0x${float_pk}/" $SEMPO_PATH/config_files/secret/local_secrets.ini

# IFS=$_IFS

#else
#  ganache-cli --db "$SEMPO_PATH/ganacheDB" &
#fi

sleep 5
MASTER_WALLET_PK=$(awk -F "=" '/master_wallet_private_key/ {print $2}' $SEMPO_PATH/config_files/secret/local_secrets.ini  | tr -d ' ')

set -e

echo ~~~Starting worker
pushd $SEMPO_PATH/eth_worker/eth_src
celery -A celery_app worker --loglevel=DEBUG --concurrency=8 --pool=gevent -Q processor,celery,low-priority,high-priority &
sleep 5
popd

echo ~~~Seeding Data
pushd $SEMPO_PATH/app/migrations/
#python -u seed.py
popd

echo ~~~Starting App
pushd $SEMPO_PATH/app
python -u run.py &
sleep 10
popd

source devtools/bancor_contract_setup_script.sh development

python devtools/add_ussd_menus.py

#python devtools/add_reserve_organisation.py tokens.json "$SEMPO_PATH/ganacheDB/keys.json"
python devtools/add_reserve_organisation.py tokens.json keys.json

python devtools/add_eth_worker_wallets.py

source devtools/add_liquid_tokens_and_converters.sh

#echo ~~~Creating Default Account
#curl -X POST 'http://localhost:9000/api/v1/auth/register/'  -H 'Content-Type: application/json' -H 'Origin: http://localhost:9000' --data-binary '{"username":"admin@acme.org","password":"C0rrectH0rse","referral_code":null}' --compressed --insecure
#psql $app_db -c 'UPDATE public."user" SET is_activated=TRUE'

#echo ~~~Setting up Contracts
##python -u devtools/contract_setup_script.py
#
#if [[ "$testdata" != 'none' ]]; then
#    echo ~~~Creating test data
#    pushd $SEMPO_PATH/app/migrations/
# #�   python -u dev_data.py ${testdata}
#    popd
#fi

echo ~~~Killing Python Processes
sleep 5
set +e
kill -9 $(ps aux | grep '[r]un.py' | awk '{print $2}')
kill -9 $(ps aux | grep '[c]elery' | awk '{print $2}')
sleep 2

#echo ~~~Done Setup! Bringing Ganache to foreground
#fg 1
