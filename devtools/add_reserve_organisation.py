import json
import sys
import os
import logging

logging.basicConfig(level=logging.DEBUG)
logg = logging.getLogger(__file__)

#old_deployment_name = os.environ['DEPLOYMENT_NAME']
#if os.environ.get('SEED_DEPLOYMENT_NAME') != None:
#    os.environ['DEPLOYMENT_NAME'] = os.environ['SEED_DEPLOYMENT_NAME']

from server import db, create_app
from server.models.token import Token, TokenType
from server.models.organisation import Organisation
from server.models.user import User

# usage $@ <token json> <ganache keys json>
if __name__ == "__main__":
    current_app = create_app()
    ctx = current_app.app_context()
    ctx.push()

    f = open(sys.argv[1], 'r')
    s = json.load(f)
    t = s['reserve']
    f.close()
    f = open(sys.argv[2], 'r')
    g = json.load(f)
    f.close()
    print(t)
    r = Token(
        address=t['address'],
        name=t['name'],
        symbol=t['symbol'],
        decimals=t['decimals'],
        token_type=TokenType.RESERVE,
            )
    db.session.add(r)
    db.session.commit()
    accounts = list(g['private'])
    logg.debug('org')
    o = Organisation(
            name='Reserve inc.',
            country_code='KE',
            token=r,
            is_master=True,
            account_address=accounts[1],
            default_disbursement=5000,
            )
    logg.debug('org commit')
    db.session.add(r)
    db.session.commit()
    
    logg.debug('user')
    u = User(
        first_name='Will E.',
        last_name='Coyote',
        blockchain_address=accounts[1],
            )
    u.create_admin_auth('admin@acme.org', 'C0rrectH0rse', tier='sempoadmin', organisation=o)
    u.is_activated = True
    logg.debug('user commit')
    db.session.add(u)
    db.session.commit()
    db.session.flush()
    #os.environ['DEPLOYMENT_NAME'] = old_deployment_name
    logg.debug('done reserve org')
