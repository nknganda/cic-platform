import json
import logging

from celery import Celery

from eth_worker.eth_src import celery_tasks

logging.basicConfig(level=logging.DEBUG)
logg = logging.getLogger(__file__)


celery_app = Celery('tasks',
        broker='redis://',
        backend='redis://',
        task_serializer='json'
        )


f = open('keys.json', 'r')
j = f.read(4096)
o = json.loads(j)

for k in o['private']:
    s = celery_app.signature(
            'celery_tasks.create_new_blockchain_wallet',
            [0, 0, k]
            )
    s.apply_async()
    logg.debug(s)
