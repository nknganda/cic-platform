#!/usr/bin/env bash

set -x

patch --forward -p1 -d contrib < patches/sempo.patch
patch --forward -p1 -d contrib < patches/ussd.patch
