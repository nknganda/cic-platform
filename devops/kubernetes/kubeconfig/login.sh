#!/bin/bash
RANCHERENDPOINT=https://$ENDPOINT/v3
# Username, password of the user
USERNAME=
PASSWORD=

# Login as user and get usertoken
LOGINRESPONSE=`curl -s $RANCHERENDPOINT-public/localProviders/local?action=login -H 'content-type: application/json' --data-binary '{"username":"'$USERNAME'","password":"'$PASSWORD'"}' --insecure`
USERTOKEN=`echo $LOGINRESPONSE | jq -r .token`
echo $USERTOKEN
# Set `token` in kubeconfig
