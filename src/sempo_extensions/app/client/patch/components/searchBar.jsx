import React from "react";
import { connect } from "react-redux";
import { ModuleBox, StyledButton } from "../../components/styledElements";
import AsyncButton from "../../components/AsyncButton";
import styled from "styled-components";
import { browserHistory } from "../../createStore.js";


class SearchBar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user_phone: ''
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleReset = this.handleReset.bind(this)
        this.searchTransferAccount = this.searchTransferAccount.bind(this)
    }

    handleChange(event){
        this.setState({ [event.target.name ]: event.target.value })
    }

    handleReset(){
        this.setState({
            user_phone: ''
        })
    }

    searchTransferAccount(){
        let userPhone;

        // check that input is not null
        if (this.state.user_phone !== '') {
            userPhone = encodeURIComponent(this.state.user_phone)
        }

        // pass value to parent for dispatch action
        this.props.fetchSearchedTransferAccount(userPhone)

        this.handleReset()
    }


    render(){
        return (
            /*Define search bar and accompanying button*/
            <div>
                <div>
                  <StyledButton
                      onClick={() =>
                          browserHistory.push(
                              "/create?type=" + browserHistory.location.pathname.slice(1)
                          )
                      }
                      className="btn-small"
                  >
                      Add User
                  </StyledButton>
                </div>

                <ModuleBox
                    style={{
                        height: "30vh",
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "center",
                        alignItems: "center"
                    }}
                >
                    <SearchWrapper>
                        <div className="form-group">
                            <SearchInput
                                style={{
                                    width: "50vw"
                                }}
                                value={ this.state.user_phone }
                                onChange={ this.handleChange }
                                className="form-control"
                                name="user_phone"
                                placeholder="Enter user phone"
                            />
                        </div>

                        <AsyncButton
                            miniSpinnerStyle={{ height: "10px", width: "10px" }}
                            onClick={ this.searchTransferAccount }
                            buttonText={"SEARCH"}
                        />
                    </SearchWrapper>
                </ModuleBox>
            </div>

        )
    }
}

export default connect()(SearchBar)

const SearchWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 0.5em;
`;

const SearchInput = styled.input`
  border: solid #a9a9a9;
  border-width: 2px;
  padding: 1em 1em 1em 40px;
  background: transparent;
  &:focus {
    border-color: #2d9ea0;
  }
`;
