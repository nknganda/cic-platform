# platform imports
from server.models.user import User

def create_user(phone, organisation, **kwargs):
    """
    This method creates a user without a transfer account or blockchain wallet.
    :param phone: string with user's msisdn
    :return: User
    """
    temporary_first_name = kwargs.get('first_name')
    temporary_last_name = kwargs.get('last_name')
    user = User(first_name=temporary_first_name,
                last_name=temporary_last_name,
                phone=phone)
                #registration_method=RegistrationMethodEnum.USSD_SIGNUP)

    if organisation == None:
        raise ValueError('organisation must be specified')

    user.add_user_to_organisation(organisation, False)
    return user


#def attach_transfer_account_to_user(user):
#    """
#    This method takes a user object argument, ideally one created through the ussd self sign up flow
#    https://docs.google.com/document/d/1UwGcNUFIlrRgpZiGkhMITeu_uqR7i3VcSRw_V89_q2c/edit (which should not have
#    a transfer account or wallet), it then creates a blockchain wallet whose address to attaches to the user object
#    as well as a transfer account that takes the created wallets's address, the user and an organization as arguments an
#    binds the account to the user.
#    The method also takes an organization object as an argument to match the user object to a specific organization.
#
#    :param user: The initial account the user created when they began the self sign up process.
#    :return: A user with a transfer account,
#    """
#
#    if user.primary_blockchain_address:
#        raise Exception('User already has a transfer account attached.')
#
#    organisation = g.active_organisation
#    if not organisation:
#        raise Exception("No active organisation")
#
#    blockchain_address = bt.create_blockchain_wallet()
#    user.primary_blockchain_address = blockchain_address
#    user.set_held_role('BENEFICIARY', 'beneficiary')
#    temporary_gender = 'Unknown gender'
#    temporary_business_directory = 'Unknown business'
#    temporary_location = 'Unknown location'
#
#    db.session.add(user)
#
#    transfer_account = TransferAccount(bound_entity=user,
#                                       organisation=organisation,
#                                       blockchain_address=blockchain_address)
#    db.session.add(transfer_account)
#    user.default_transfer_account = transfer_account
#    attrs = {
#        "custom_attributes": {
#            "bio": temporary_business_directory,
#            "gender": temporary_gender
#        }
#    }
#    user.location = temporary_location
#    set_custom_attributes(attrs, user)
#
#    initial_disbursement = config.SELF_SERVICE_WALLET_INITIAL_DISBURSEMENT
#
#    initial_disbursement = make_payment_transfer(transfer_amount=initial_disbursement,
#                                                 send_transfer_account=organisation.org_level_transfer_account,
#                                                 receive_user=user,
#                                                 transfer_subtype=TransferSubTypeEnum.DISBURSEMENT)
#    db.session.add(initial_disbursement)
#
#    return user
