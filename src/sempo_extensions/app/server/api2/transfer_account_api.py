# standard imports
import logging

# framework imports
from server import db
from flask import Blueprint, request, make_response, jsonify, g, Response
from flask.views import MethodView
from server.models.transfer_account import TransferAccount
from server.models.organisation import Organisation
from server.models.user import User
from server.schemas import transfer_account_schema, view_transfer_account_schema
from server.utils.auth import requires_auth
from server.utils.access_control import AccessControl

logg = logging.getLogger(__file__)

transfer_account_blueprint = Blueprint('v2_transfer_account', __name__)


class TransferAccountAPI(MethodView):

    def post(self):
        organisation_id = None
        address = None

        post_data = request.get_json()
        try:
            address = post_data['address']
        except:
            response_object = {
                'message': 'Missing address',
            }
            return make_response(jsonify(response_object)), 400
        try:
            organisation_id = post_data['organisation_id']
        except:
            response_object = {
                'message': 'Missing organisation',
            }
            return make_response(jsonify(response_object)), 400

        # TODO: when exchange sync will automatically update balance this should be removed
        initial_balance_wei = post_data.get('initial_balance_wei', 0)
        
        organisation = Organisation.query.get(organisation_id)
        if organisation == None:
            response_object = {
                'message': 'Organisation "{}" not found'.format(organisation_id),
            }
            return make_response(jsonify(response_object)), 404


        ta = TransferAccount(
                bound_entity = None,
                blockchain_address = address,
                _balance_wei = initial_balance_wei,
                )
        ta.organisation = organisation
        ta.token = organisation.token
        db.session.add(ta)

        response_object = {
            'message': 'Created',
            'data': {
                'id': ta.id,
                }
        }
        return make_response(jsonify(response_object)), 201


class SearchTransferAccountAPI(MethodView):
    @requires_auth(allowed_roles={'ADMIN': 'any'})
    def get(self):
        result = None
        # This method is a patch to the user API to try and return transfer account based on a user's phone number.
        user_phone = request.args.get('user_phone')
        if user_phone:
            user = User.query.filter_by(phone=user_phone).first()
            if user:
                transfer_account = user.default_transfer_account

                if AccessControl.has_sufficient_tier(g.user.roles, 'ADMIN', 'admin'):
                    result = transfer_account_schema.dump(transfer_account)
                elif AccessControl.has_any_tier(g.user.roles, 'ADMIN'):
                    result = view_transfer_account_schema.dump(transfer_account)

                response_object = {
                    'message': 'Successfully loaded transfer account.',
                    'data': {'transfer_account': result.data}
                }
                return make_response(jsonify(response_object)), 200
            else:
                response_object = {
                    'message': 'No user matching that phone number could be found.'
                }
                return make_response(jsonify(response_object)), 404
        else:
            response_object = {
                'message': 'No phone number provided.'
            }
            return make_response(jsonify(response_object)), 422


transfer_account_blueprint.add_url_rule(
    '/transfer_account/register/',
    view_func=TransferAccountAPI.as_view('v2_transfer_account_register_view'),
    methods=['POST'],
)

transfer_account_blueprint.add_url_rule(
    '/search_transfer_account/',
    view_func=SearchTransferAccountAPI.as_view('search_transfer_account_view'),
    methods=['GET']
)
