# standard imports
import pytest

# platform imports
from server import db
from server.models.exchange import ExchangeContract
from server.models.token import Token

# these values are totally arbitrary
liquid_token_address = '0xDEBA63D7766E1963755EFF6F216490D652EBA'
reserve_token_address = '0x99E8D9415C52DEA3D31EBA639326A823D3765E'
converter_address = '0xF8A3501FBDAF3CE2F3B44C4A6F481FE9D8E34D44'
registry_address = '0xFE5784AF7C78A0BEDF87724D9DF3D29C8B985553'


def test_register_exchange_contract(
        test_client,
        init_database,
        create_master_organisation,
        ):


    rt = Token(address=reserve_token_address, name='reserve token', symbol='RESERVE', token_type='RESERVE', decimals=18) 
    lt = Token(address=liquid_token_address, name='cic token', symbol='CIC', token_type='LIQUID', decimals=18) 
    db.session.add(rt)
    db.session.add(lt)
    db.session.commit()
    e = ExchangeContract(
            blockchain_address=converter_address,
            contract_registry_blockchain_address=registry_address,
            reserve_token_id=rt.id,
            )
    db.session.add(e)
    db.session.commit()
    e.add_token(lt, converter_address, 250000)
    db.session.add(e)
    db.session.commit()
