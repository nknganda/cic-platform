"""
Use git repository to retrieve files.
"""

# standard imports
import os
import logging
import tempfile
from urllib.parse import urlparse, ParseResult

# third-party imports
from git import Repo

# platform imports
from files.sync import FileSyncer

logg = logging.getLogger(__name__)


class Git(FileSyncer):
    """
    Provides access to files stored on a git repository.
    This class takes a destination path that defines where to write the files from Gitlab.
    """

    def __init__(self, destination_path, source_path, **kwargs):
        t = tempfile.TemporaryDirectory()
        self._t = t
        logg.debug('git source {} will be cloned to {}'.format(source_path, t.name))
        u = urlparse(source_path)

        # add authentication if set
        if kwargs.get('username') != None:
            netloc = u.netloc
            if kwargs.get('password') != None:
                netloc = '{}:{}@{}'.format(kwargs['username'], kwargs['password'], netloc)
            else:
                netloc = '{}@{}'.format(kwargs['username'], netloc)
            u = ParseResult(
                    u.scheme,
                    netloc,
                    u.path,
                    u.params,
                    u.query,
                    u.fragment)
                    
        Repo.clone_from(u.geturl(), t.name)
        source_path_part = os.path.join(t.name, kwargs.get('source_sub_path'))
        super(Git, self).__init__(source_path_part, destination_path, getfunc=self.get_files)


    def source_is_newer(self, remote_filepath, local_filepath):
        return True


    def get_files(self, filename: str):
        """
        This function issues a get request for the file from the Gitlab Files API and returns a string containing the file's
        data. It then collects the file content and the last commit id for the files repo.
        :param filename: The name of the file to retrieve from the repo
        :return: string
        """

        return open(os.path.join(self.source_path, filename), 'rb')
