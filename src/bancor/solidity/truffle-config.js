const HDWalletProvider = require('@truffle/hdwallet-provider');

/* eslint-disable import/no-extraneous-dependencies */
require('chai')
    .use(require('chai-as-promised'))
    .use(require('chai-bn')(require('bn.js')))
    .use(require('chai-string'))
    .use(require('dirty-chai'))
    .expect();
const fs = require('fs');

//const Decimal = require('decimal.js');
//Decimal.set({ precision: 100, rounding: Decimal.ROUND_DOWN, toExpPos: 40 });

//const ganache = require('ganache-core');
/* eslint-enable import/no-extraneous-dependencies */

const mnemonic = process.env['ETH_MNEMONIC'] //"history stumble mystery avoid embark arrive mom foil pledge keep grain dice";
console.debug('using mnemonic', mnemonic);
//const mnemonic = fs.readFileSync('../compromised_mnemonic.txt', {
//	encoding: 'utf-8',
//});

console.log('mnemonic ', mnemonic);
//test: {
//            host: 'localhost',
//            port: 7545,
//            network_id: '*',
//            gasPrice: 20000000000,
//            gas: 9500000,
//            provider: ganache.provider({
//                gasLimit: 9500000,
//                gasPrice: 20000000000,
//                default_balance_ether: 10000000000000000000
//            })
//        },
module.exports = {
    networks: {
        
	development: {
     	    provider: () => 
        	new HDWalletProvider(mnemonic, 'http://localhost:8545'),
	    network_id: '42',
            gasPrice: 1000000000,
            gas: 8000000
	},
	bloxberg: {
            host: 'blockexplorer.bloxberg.org/api/eth_rpc',
            port: 443,
      	    network_id: '8995', 
            gasPrice: 1000000000,
            gas: 8000000
        },
	bloxberg_development: {
      		provider: () => 
        		new HDWalletProvider(mnemonic, 'https://blockexplorer.bloxberg.org/api/eth_rpc'),
      		network_id: '8995', 
		gasPrice: 1000000000,
		gas: 8000000
    	},

    },
    plugins: ['solidity-coverage'],
    compilers: {
        solc: {
            version: '0.6.12',
            settings: {
                optimizer: {
                    enabled: true,
                    runs: 200
                }
            }
        }
    },
    mocha: {
        before_timeout: 600000,
        timeout: 600000,
        useColors: true,
        reporter: 'list'
    }
};
