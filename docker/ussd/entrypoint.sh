#!/usr/bin/env bash

pushd /app/src

echo updating resource files
python init.py

ret=$?
if [ "$ret" -ne 0 ]; then
  exit $ret
fi

if [ "$CONTAINER_MODE" = 'TEST' ]; then
  coverage run invoke_tests.py
  ret=$?
  if [ "$ret" -ne 0 ]; then
    exit $ret
  fi
else
  uwsgi --socket 0.0.0.0:9000 --protocol http  --processes 4 --enable-threads --module=server.wsgi:app --stats :3031 --stats-http
fi
