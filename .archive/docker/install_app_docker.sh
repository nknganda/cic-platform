#!/bin/bash

# Author: Louis Holbrook <dev@holbrook.no> https://holbrook.no
# License: GPLv3
# GPG: 0826EDA1702D1E87C6E2875121D2E7BB88C2A746
# Description: Create a 100% clean deployment of the sarafu platform for use in development
#
# Script is not in safe state and may not exit cleanly; you might have to manually kill celery and ganache
# adaptation of install_app.sh in repository root, for use in docker build


if [ ! -z "$1" ]; then
	if [ ! -d "$1" ]; then
		>&2 echo arg 2 must be a directory
		exit 1
	fi
fi

deps=(jq)

# dirs and paths

d=`realpath $(dirname ${BASH_SOURCE[0]})`
pushd $d/..
d=`pwd`
d_log=$d/var/log/grassroots
d_lib=$d/var/lib/grassroots
d_run=$d/var/run/grassroots
d_tools=$d/bin

for p in `find $d_run -iname '*.pid'`; do
	pp=`cat $p`
	echo "killing pid $pp"
	kill -TERM $pp
done

mkdir -vp $d_log
mkdir -vp $d_lib
mkdir -vp $d_run

. .venv/bin/activate

sempodir=$SEMPO_PATH
sempooverridedir=$SEMPO_OVERRIDE_APP_PATH
export PYTHONPATH=$sempodir:$sempodir/eth_worker/eth_manager:$sempodir/eth_worker:$sempodir/eth_worker/eth_manager/task_interfaces:$PYTHONPATH

# verify tool dependencies

# node version must match bancor requirement
node_bin=`which node`
if [ "$?" -gt 0 ]; then
	>&2 echo "node not found"
	exit 1
fi
node_version=`node --version`
node_version_bancor="10.16.0"
if [ "$node_version" != "v${node_version_bancor}" ]; then
	nvm_bin=`which nvm`
	if [ $? -gt 0 ]; then
		>&2 echo "bancor needs node version $node_version_bancor. Maybe nvm can help you out?"
		exit 1	
	fi
	nvm use 10.16.0
	if [ $? -gt 0 ]; then
		>&2 echo "nvm could not switch to node version $node_version_bancor which is needed by bancor"
		exit 1	
	fi
fi


# prepare the blockchainy part

bancor_dir=$(realpath ${BANCOR_PATH:-$1})
echo "bancodir $bancor_dir"
if [ -z $bancor_dir ]; then
	bancor_dir=$d/contrib/bancor
fi
if [ ! -d $bancor_dir ]; then
	>&2 echo "bancor dir '$bancor_dir' not a dir"
	exit 1
fi


pushd $bancor_dir
bancor_commit=`git rev-parse HEAD`
if [ "$bancor_commit" != "$BANCOR_COMMIT" ]; then
	>&2 echo "wrong bancor version, need $BANCOR_COMMIT, have $bancor_commit"
	exit 1
fi
if [ ! -d "node_modules" ]; then # risky, doesn't check the contents
	# snyk never completes..
	mv package.json package.json.tmp
	jq '.scripts.prepare = "echo skipping prepare snyk because it just neeeever finishes :/"' package.json.tmp | jq '.snyk = false' > package.json
	rm package-lock.json
	npm install
	mv package.json.tmp package.json
fi
if [ ! -f ${bancor_dir}/node_modules/truffle/build/cli.bundled.js ]; then
	>&2 echo "cannot find truffle bin"
	exit 1
fi
truffle=${bancor_dir}/node_modules/truffle/build/cli.bundled.js
pushd solidity

$truffle --network grassroots_docker_development migrate --reset
if [ "$?" -gt 0 ]; then
	>&2 echo "truffle migrate fail"
	exit 1
fi
popd
popd


# APP
#
# generate new configs from template
# note in section public/local_config.ini;
# contract addresses will always be the same if deployed with same source and same network settings in ganache
#
# TODO: use different deployment name

master_private_key=`jq -r '.private_keys[]' ${d_lib}/ganache.accounts | head -n 1`
master_address=`python ${d}/tools/ethereum_checksum_address.py $(jq -r '.private_keys | to_entries[0].key' ${d_lib}/ganache.accounts)`
echo private key is $master_private_key
# todo move config to etc
#echo sempodir $sempodir
#rm -rf $sempodir/config_files/secret
#mkdir -vp $sempodir/config_files/secret
#pushd $sempodir/config_files
#cp -v $d/usr/share/sempo/gedocker_secrets.ini secret/
#cp -v $d/usr/share/sempo/gedocker_config.ini public/
#popd


# migrate database to latest version

pushd $sempodir/app
python manage.py db upgrade
#alembic upgrade head
if [ "$?" -gt "0" ]; then
	>&2 echo "db migration fail"
	popd
	exit 1
fi
popd
pushd $sempodir/eth_worker
alembic upgrade head


pushd $sempodir/app/migrations 
# TODO replace with curl calls in this script
echo -e "\n>>> STARTING SEED SCRIPT\n"
tfatoken=`python3 seed.py ${APP_PROTO}://${APP_HOST}:${APP_PORT} ${LOCAL_EMAIL} ${LOCAL_PASSWORD} 0x0ec5dE3E0c2d5B5f7692C8ee92Df2ea8088941d3 | tail -n 1 | cut -b 3- | tr "'" " " | sed -e 's/ //'`
if [ "$?" -gt 0 ]; then
	>&2 echo seed script failed
	popd
	exit 1
fi
popd



#curlflags='-v'
curlflags=''
api_url="http://${APP_HOST}:${APP_PORT}/api/v1"
api_url_ge="http://${APP_HOST}:${APP_PORT}/api/v1/ge"
curl ${curlflags} -X POST ${api_url}/auth/register/ -d "{\"email\":\"${LOCAL_EMAIL}\",\"password\":\"${LOCAL_PASSWORD}\"}" -H 'Content-Type: application/json'


# hack to activate user 1 to enable API calls against it
# TODO: activate through API instead if possible

export PGPASSWORD=${DATABASE_PASSWORD}
psql -U postgres -h ${DATABASE_HOST} -d grassroots -c 'UPDATE public.user SET is_activated = true WHERE id = 1;'
popd



contract_names=()
contract_addresses=()
token_address=''
reserve_address=''
registry_address=''
i=0
for f in `ls $BANCOR_PATH/solidity/build/contracts/*.json`; do
	k=${f%%.*}
	k=${k##*/}
	j=`jq -r '.networks["42"].address' $f`
	if [ "$j" != 'null' ]; then
		address=`python ${d}/tools/ethereum_checksum_address.py ${j}`
		if [ "$address" == 'null' ]; then
			continue
		fi
		echo $k $address
		contract_names[i]=$k
		contract_addresses[i]=$address
		if [ "$k" == "SmartToken" ]; then
			token_address=$address
		fi
		if [ "$k" == "EtherToken" ]; then
			reserve_address=$address
		fi
		if [ "$k" == "ContractRegistry" ]; then
			registry_address=$address
		fi
		i=$((i+1))
	fi
done
echo ${contract_names[@]}
echo ${contract_addresses[@]}

converter_address=`python ${d}/tools/bancor_converter_registry.py --endpoint $WEB3_URL --bancor-dir $BANCOR_PATH --registry-address $registry_address $token_address`
pushd ${sempodir}/config_files
sed -i "/^\[ETHEREUM\]$/,/^\[$/ s/^contract_address[[:space:]].*$/contract_address = ${registry_address}/" public/${DEPLOYMENT_NAME}_config.ini
sed -i "/^\[ETHEREUM\]$/,/^\[$/ s/^contract_address[[:space:]].*$/contract_address = ${registry_address}/" public/${DEPLOYMENT_NAME}_config.ini
popd

d=`cat <<EOF
{
	"email": "$LOCAL_EMAIL",
	"password": "$LOCAL_PASSWORD"
}
EOF
`
echo $d | jq
r=`curl  -X POST ${api_url}/auth/request_api_token/ -d "$d" -H "Content-Type: application/json" | jq -r .auth_token`
auth_key=$r
echo "got auth key $auth_key"

# todo get names from contract jsons
d=`cat <<EOF
{
	"name": "Grassroots XDAI Reserve Token",
	"symbol": "XDAIGR",
	"decimals": 18,
	"is_reserve": true,
	"address": "$reserve_address"
}
EOF
`
echo $d | jq
r=`curl ${curlflags} -X POST ${api_url}/token/ -d "$d" -H "Authorization: ${auth_key}" -H "Accept: application/json" -H "Content-Type: application/json" --basic -u "$LOCAL_EMAIL:$LOCAL_PASSWORD"`
echo $r

d=`cat <<EOF
{
	"name": "Sarafu",
	"symbol": "SFU",
	"decimals": 18,
	"is_reserve": false,
	"address": "$token_address"
}
EOF
`
echo $d | jq
r=`curl  ${curlflags} -X POST ${api_url}/token/ -d "$d" -H "Authorization: ${auth_key}" -H "Accept: application/json" -H "Content-Type: application/json" --basic -u "$LOCAL_EMAIL:$LOCAL_PASSWORD"`
echo $r
token_id=`echo $r | jq '.data.token.id'`
echo "got token id $token_id"

d=`cat <<EOF
{
	"reserve_address": "$reserve_address",
	"token_address": "$token_address",
	"exchange_address": "$converter_address",
	"registry_address": "$registry_address",
	"reserve_ratio_ppm": 250000
}
EOF
`
echo $d | jq
r=`curl ${curlflags} -X PUT ${api_url_ge}/exchange/ -d "$d" -H "Authorization: ${auth_key}" -H "Accept: application/json" -H "Content-Type: application/json" --basic -u "$LOCAL_EMAIL:$LOCAL_PASSWORD"`
echo $r
exchange_id=`echo $r | jq '.data.exchange.id'`
echo "got exchange id $exchange_id"

d=`cat <<EOF
{
        "organisation_name": "Grassroots Economics",
        "custom_welcome_message_key": "grassroots",
        "timezone": "Africa/Nairobi",
        "country_code": "AU",
        "exchange_contract_id": $exchange_id,
	"token_id": 2
}
EOF
`
echo $d | jq
r=`curl ${curlflags} -X POST ${api_url}/organisation/ -d "$d" -H "Authorization: ${auth_key}" -H "Accept: application/json" -H "Content-Type: application/json" --basic -u "$LOCAL_EMAIL:$LOCAL_PASSWORD"`
echo $r
organisation_id=`echo $r | jq '.data.organisation.id'`

d=`cat <<EOF
{
	"user_ids": [1],
	"is_admin": true
}
EOF
`
#echo $d | jq
#r=`curl ${curlflags} -X PUT ${api_url}/organisation/${organisation_id}/users -d "$d" -H "Authorization: ${auth_key}" -H "Accept: application/json" -H "Content-Type: application/json" --basic -u "$LOCAL_EMAIL:$LOCAL_PASSWORD"`
#echo $r

echo $d | jq
r=`curl ${curlflags} -X PUT ${api_url}/organisation/1/users -d "$d" -H "Authorization: ${auth_key}" -H "Accept: application/json" -H "Content-Type: application/json" --basic -u "$LOCAL_EMAIL:$LOCAL_PASSWORD"`
echo $r
