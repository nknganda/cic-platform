#!/bin/bash

# Author: Louis Holbrook <dev@holbrook.no> https://holbrook.no
# License: GPLv3
# Description: Sets up virtualenv with the correct python interpreter and installs module dependencies for python and node
# 
# push all grassrootseconomics docker images to current repository

i=0
for l in `docker image ls | grep "^grassrootseconomics/" | awk '{ print $1 ":" $2 ; }'`; do
	i=$((i+1))
	echo $i $l
	docker push $(echo -n $l)
done
